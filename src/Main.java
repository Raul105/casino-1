
//Imports
import javax.swing.text.Style;
import java.util.Scanner;
import java.util.Random;

public class Main {

   public static void main(String[] args) {
      int selection = 0;
      int multiplyerAmount = 0;
      int winnings = 0;
      do
      {
         selection = getBet();
         ThreeString thePull = pull();
         multiplyerAmount = getPayMultiplier(thePull);
         winnings = selection * multiplyerAmount;

         display(thePull, winnings);
         thePull.saveWinnings(winnings);
         thePull.displayWinnings();


      } while ( selection !=0 );

      System.out.print("We call display winnings now!");
   }

   public static int getBet()
   {
      int bet;
      Scanner keyboard = new Scanner(System.in);
      System.out.print("How much would you like to bet (1 - 100) or 0 to quit? " );
      bet = keyboard.nextInt();
      while ( bet < 0 || bet > 100)
      {
         System.out.print("You did not enter a valid number, please choose between 1-100, 0 to quit.");
         bet = keyboard.nextInt();
      }

      return bet;
   }

   public static String randString()
   {
      Random ran = new Random();
      String result;
      int randomInt = ran.nextInt(8);

      if ( randomInt == 0 || randomInt == 1 || randomInt == 2 ||randomInt == 3)
      {
         result = "space";
      }
      else if ( randomInt == 4 || randomInt == 5)
      {
         result = "cherries";
      }
      else if ( randomInt == 6)
      {
         result = "BAR";
      }
      else
      {
         result = "7";
      }

      return result;
   }

   public static ThreeString pull()
   {
      ThreeString item = new ThreeString();
      item.setString1(randString());
      item.setString2(randString());
      item.setString3(randString());
      return item;
   }

   public static int getPayMultiplier(ThreeString thePull)
   {
      String string1 = thePull.getString1();
      String string2 = thePull.getString2();
      String string3 = thePull.getString3();
      int multiplier = 0;

      if (string1 == "cherries" && string2 != "cherries")
      {
         multiplier = 5;
      }

      if (string1 == "cherries" && string2 == "cherries" && string3 != "cherries")
      {
         multiplier = 15;
      }

      if (string1 == "cherries" && string2 == "cherries" && string3 == "cherries")
      {
         multiplier = 30;
      }

      if (string1 == "BAR" && string2 == "BAR" && string3 == "BAR")
      {
         multiplier = 50;
      }

      if (string1 == "7" && string2 == "7" && string3 == "7" )
      {
         multiplier = 100;
      }

      return multiplier;
   }

   public static void display(ThreeString thePull, int winnings)
   {
      if (winnings > 0)
      {
         System.out.println(thePull.toString(thePull.getString1(), thePull.getString2(), thePull.getString3()));
         System.out.println("Congratulations you won $" + winnings);
      }
      else
      {
         System.out.println(thePull.toString(thePull.getString1(), thePull.getString2(), thePull.getString3()));
         System.out.println("Sorry you lost, try again.");
      }
   }

}

class ThreeString
{
   private String string1, string2, string3;
   private static final int MAX_PULLS = 40;
   public static final int MAX_LEN = 20;
   private static int[] pullWinnings = new int[MAX_PULLS];
   public static int numPulls = 0;

   public ThreeString()
   {
      this.string1 = " ";
      this.string2 = " ";
      this.string3 = " ";
   }

   public boolean setString1( String input )
   {
      //todo: Need to filter out bad strings, call validString()
      this.string1 = input;
      return true;
   }
   public boolean setString2 (String input )
   {
      //todo: Need to filter out bad strings, call validString()
      string2 = input;
      return true;
   }
   public boolean setString3 (String input)
   {
      //todo: Need to filter out bad strings, call validString()
      string3 = input;
      return true;
   }
   public String getString1()
   {
      return string1;
   }
   public String getString2()
   {
      return string2;
   }
   public String getString3()
   {
      return string3;
   }

   public boolean saveWinnings(int winnings)
   {
      this.pullWinnings[numPulls]= winnings;
      this.numPulls++;
      return true;
   }

   public String displayWinnings()
   {
      String message = "Your individual winnings are: ";
      for(int i =0; i < numPulls; i++)
      {
         System.out.print(pullWinnings[i] + " ");
      }
      return message;
   }

   public String toString(String input1, String input2, String input3)
   {
      String completeString = input1 + "  " + input2 + "  " + input3;
      return completeString;
   }
}